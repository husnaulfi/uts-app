package ulfiah.muhlishoh.appauthetication

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_signin.*
import java.util.*


class SignInActivity : AppCompatActivity() {
    var fbAuth = FirebaseAuth.getInstance()
    var fbDb = FirebaseDatabase.getInstance()
    var fbStore = FirebaseStorage.getInstance().getReference()
    var tab = fbDb.getReference("LIST")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnLogoff.setOnClickListener {
            fbAuth.signOut()
            Toast.makeText(this, "Berhasil Log Out", Toast.LENGTH_SHORT).show()
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        btnUpload.setOnClickListener {
            selectFile()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 111 && resultCode == RESULT_OK) {
            val selectedFile = data?.data //The uri with the location of the file
            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.form_layout)
            val body = dialog.findViewById(R.id.body) as TextView
            val yesBtn = dialog.findViewById(R.id.yesBtn) as Button
            val noBtn = dialog.findViewById(R.id.noBtn) as Button
            var act  = dialog.findViewById(R.id.aktifitas) as EditText
            val file = dialog.findViewById(R.id.file) as TextView
            body.text = "Upload"
            file.text = selectedFile.toString()
            val img = selectedFile
            var upUrl = "images/"+ Random() +".jpg"

            val riversRef: StorageReference = fbStore.child(upUrl)

            yesBtn.setOnClickListener {
                val aktifitas = act.text.toString();
                if (img != null) {
                    riversRef.putFile(img)
                        .addOnSuccessListener { result -> // Get a URL to the uploaded content

                                val user = Users(aktifitas,upUrl)
                                val tabId = tab.push().key.toString()
                                tab.child(tabId).setValue(user).addOnCompleteListener {

                                    Toast.makeText(this,"Upload Success at " + upUrl,Toast.LENGTH_SHORT).show()
                                }

                        }
                        .addOnFailureListener {
                            Toast.makeText(this,"Upload Failed",Toast.LENGTH_SHORT).show()
                        }
                }
                dialog.dismiss()
            }
            noBtn.setOnClickListener { dialog.dismiss() }
            dialog.show()
        }
    }
    private fun selectFile() {

        val intent = Intent()
            .setType("*/*")
            .setAction(Intent.ACTION_GET_CONTENT)

        startActivityForResult(Intent.createChooser(intent, "Select a file"), 111)
    }

}